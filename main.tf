resource "aws_instance" "ec2_public" {
  ami                         = "${var.image}"
  associate_public_ip_address = true
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key}"
  subnet_id                   = "${var.public_subnet[0]}"
  

  tags = {
    Name = "${var.server_name}"
  }

}
